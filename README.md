# Stripe-laravel



## Getting started

Here we are integrating stripe with laravel to add customers directly on stripe account.

## Install Stripe-PHP SDK using composer

	composer require stripe/stripe-php

## Getting started with Stripe (Authentication)

Use your API key by setting it in the initial configuration of new \Stripe\StripeClient(). The PHP library will then automatically send this key in each request.

	$stripe = new \Stripe\StripeClient("YOUR_STRIPE_SECRET_KEY");

## Create Csutomer

	//Initialise stripe
        
        $stripe = new \Stripe\StripeClient(env("YOUR_STRIPE_SECRET_KEY"));
        
        
        //Create Customer
        $cust = $stripe->customers->create([
            'name' => $request->cust_name,
            'email' => $request->cust_email,
            'description' => $request->cust_description
        ]);