<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Stripe;
use Stripe\Customer;
use Session;

class StripeController extends Controller
{
  


     /**
     * Add Customer View.
     *
     *
     * @return view
     */


      public function createCustomerView()
    {
        return view('create_customer');
    }


    


    /**
     * Create a customer - Stripe.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return view
     */

    public function createCustomerPost(Request $request)
     {  

        //Initialise stripe
        
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
        
        // validate the request
        $this->validate($request, [
          'cust_name' => 'required',
          'cust_email' => 'required|email',
          'cust_description' => 'required'
        ]);

        //Create Customer
        $cust = $stripe->customers->create([
            'name' => $request->cust_name,
            'email' => $request->cust_email,
            'description' => $request->cust_description
        ]);

        return back()->with('success', 'Customer added successfully.');

    }
}

