@extends('layout.mainlayout')
@section('content')
<div class="content-wrapper">

    <div class="row">

        <div class="col-lg-12 card-margin">

            <div class="card">

                <div class="card-header">

                    <h5 class="card-title">CREATE CUSTOMER</h5>

                </div>

                <div class="card-body"></div>

                <div class="panel-body">

                    @if(Session::has('success'))
                    <div class="alert alert-success text-center">
                    {{Session::get('success')}}
                    </div>
                    @endif  

                    <form role="form" action="{{ route('stripe.createCustomerPost') }}" method="post" class="validation"
                     data-cc-on-file="false"
                    data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                    id="customer-form">
                    @csrf

                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name</label> 
                                <input
                                class="form-control @error ('cust_name') is-invalid @enderror cust_name" name="cust_name" id="cust_name" size='54' type='text'>

                                @error('cust_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Email</label> 
                                <input
                                class="form-control @error ('cust_email') is-invalid @enderror cust_email" name="cust_email" id="cust_email" size='54' type='text'>
                                @error('cust_email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Description</label> 
                                <input
                                class="form-control @error ('cust_description') is-invalid @enderror cust_email cust_description" name="cust_description" id="cust_description" size='54' type='text'>
                                @error('cust_description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <button class="btn btn-danger btn-lg btn-block" type="submit">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
</div>
@endsection






